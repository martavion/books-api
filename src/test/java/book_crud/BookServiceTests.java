package book_crud;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import books_api.dto.BookDto;
import books_api.models.Book;
import books_api.models.Genre;
import books_api.repositories.BookRepository;
import books_api.services.BookService;
import books_api.services.GenreService;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest(classes = BookServiceTests.class)
@TestInstance(PER_CLASS)
public class BookServiceTests {

    @MockBean
    private BookRepository bookRepositoryMock;

    @MockBean
    private GenreService genreServiceMock;

    @Captor
    ArgumentCaptor<Book> bookArgumentCaptor;

    private BookService bookService;

    @BeforeAll
    public void init() {
        bookService = new BookService(bookRepositoryMock, new ModelMapper(), genreServiceMock);
    }

    @Test
    @DisplayName("Create book and return book id")
    void shouldCreateBookAndReturnBookId() {
        //given
        Book book = new Book.Builder()
                .withBookId(256l)
                .withAuthor("author1")
                .withTitle("title1")
                .withPublishingYear(1985)
                .withGenre(new Genre(1l, "non fiction"))
                .withCreatedAt(new Date())
                .withUpdatedAt(new Date())
                .build();

        BookDto bookDto = new BookDto.Builder()
                .withAuthor("author1")
                .withTitle("title1")
                .withPublishingYear(1985)
                .withGenre("non fiction")
                .build();
        //when
        when(bookRepositoryMock.save(any(Book.class))).thenReturn(book);
        Long bookId = bookService.create(bookDto);
        //then
        assertNotNull(bookId);
        assertEquals(book.getBookId(), bookId);

    }

    @Test
    @DisplayName("Retrieve book by id and return book DTO")
    void shouldGetBookByIdAndReturnBookDto() {
        //given
        Book book = new Book.Builder()
                .withBookId(256l)
                .withAuthor("author2")
                .withTitle("title2")
                .withPublishingYear(1985)
                .withGenre(new Genre(2l, "fiction"))
                .withCreatedAt(new Date())
                .withUpdatedAt(new Date())
                .build();

        BookDto expectedBookDto = new BookDto.Builder()
                .withBookId(256l)
                .withAuthor("author2")
                .withTitle("title2")
                .withPublishingYear(1985)
                .withGenre("fiction")
                .build();
        //when
        when(bookRepositoryMock.findById(256l)).thenReturn(Optional.of(book));

        BookDto actualBookDto = bookService.getBooksById(256l);

        //then
        assertEquals(actualBookDto.getId(), expectedBookDto.getId());
        assertEquals(actualBookDto.getAuthor(), expectedBookDto.getAuthor());
        assertEquals(actualBookDto.getAuthor(), expectedBookDto.getAuthor());
    }

    @Test
    @DisplayName("Retrieve list of books and return list of book DTOs")
    void shouldGetAllBooks() {
        //given
        Book expectedBook = new Book.Builder()
                .withBookId(256l)
                .withAuthor("author3")
                .withTitle("title3")
                .withPublishingYear(1985)
                .withGenre(new Genre(4l, "rom com"))
                .withCreatedAt(new Date())
                .withUpdatedAt(new Date())
                .build();

        //when
        when(bookRepositoryMock.findAll()).thenReturn(Collections.singletonList(expectedBook));
        List<BookDto> actualBookListDto = bookService.getAllBooks();
        //then
        assertNotNull(actualBookListDto);
        assertFalse(actualBookListDto.isEmpty());
        assertEquals(expectedBook.getBookId(), actualBookListDto.get(0).getId());
    }

    @Test
    @DisplayName("Delete book by id")
    void shouldDeleteBookById() {
        //given
        Book book = new Book.Builder()
                .withBookId(6l)
                .withAuthor("author5")
                .withTitle("title")
                .withPublishingYear(2022)
                .withGenre(new Genre(2l, "fiction"))
                .withCreatedAt(new Date())
                .withUpdatedAt(new Date())
                .build();

        //when
        when(bookRepositoryMock.findById(book.getBookId())).thenReturn(Optional.of(book));
        BookDto removedBookDto = bookService.deleteBookById(book.getBookId());

        //then
        verify(bookRepositoryMock, times(1)).deleteById(book.getBookId());
        assertNotNull(removedBookDto);
        assertNotNull(removedBookDto.getId());
        assertEquals(removedBookDto.getId(), book.getBookId());
    }

    @Test
    @DisplayName("Create book and return book id")
    void shouldUpdateBookByIdAndReturnBookDto() {

        //given
        Book book = new Book.Builder()
                .withBookId(1l)
                .withAuthor("author1")
                .withTitle("title1")
                .withGenre(new Genre(1l, "non fiction"))
                .build();

        BookDto bookDto = new BookDto.Builder()
                .withBookId(1l)
                .withAuthor("author1")
                .withTitle("title1")
                .withPublishingYear(2015)
                .withGenre("non fiction")
                .build();

        Book updatedBook = new Book.Builder()
                .withBookId(1l)
                .withAuthor("author1")
                .withTitle("title1")
                .withPublishingYear(2015)
                .withGenre(new Genre(1l, "non fiction"))
                .withCreatedAt(new Date())
                .withUpdatedAt(new Date())
                .build();

        BookDto expectedUpdatedBookDto = new BookDto.Builder()
                .withBookId(1l)
                .withAuthor("author1")
                .withTitle("title1")
                .withPublishingYear(2015)
                .withGenre("non fiction")
                .build();

        //when
        when(bookRepositoryMock.findById(bookDto.getId())).thenReturn(Optional.of(book));
        when(bookRepositoryMock.save(any(Book.class))).thenReturn(updatedBook);

        BookDto actualUpdatedBookDto = bookService.updatedBookById(1l, bookDto);

        //then
        verify(bookRepositoryMock).save(bookArgumentCaptor.capture());

        Book bookCaptorValue = bookArgumentCaptor.getValue();

        assertNotNull(bookCaptorValue.getUpdatedAt());
        assertEquals(bookCaptorValue.getPublishingYear(), updatedBook.getPublishingYear());

        verify(bookRepositoryMock, times(1)).save(book);
        assertEquals(expectedUpdatedBookDto.getAuthor(), actualUpdatedBookDto.getAuthor());
        assertEquals(expectedUpdatedBookDto.getTitle(), actualUpdatedBookDto.getTitle());
        assertEquals(expectedUpdatedBookDto.getPublishingYear(), actualUpdatedBookDto.getPublishingYear());
    }
}
