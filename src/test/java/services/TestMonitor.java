package services;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;

public class TestMonitor extends TestWatcher {
    private static final Logger logger = LogManager.getLogger(TestMonitor.class);
    private long start;
    private long duration;


    protected void succeeded(Description description) {
        logger.info(description.getMethodName() + " " + "is PASSED");
    }

    protected void failed(Throwable e, Description description) {
        logger.error(description.getMethodName() + " " + "is FAILED");

    }

    protected void skipped(org.junit.AssumptionViolatedException e, Description description) {
        logger.warn(description.getMethodName() + " " + "is SKIPPED");
    }

    protected void starting(Description description) {
        logger.info(description.getMethodName() + " " + "is starting...");
        start = System.currentTimeMillis();
    }

    protected void finished(Description description) {
        duration = System.currentTimeMillis() - start;
        logger.info("Duration: " + duration + " ms");
        
    }
}
