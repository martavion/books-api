package books_api.exception_handlers.error_responses;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class BookErrorResponse {

    @JsonProperty("errors")
    private List<String> messageList = null;

    @JsonProperty("timestamp")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime timestamp;

    public BookErrorResponse() {
    }

    public BookErrorResponse(String message, LocalDateTime timestamp) {
        this.messageList = Collections.singletonList(message);
        this.timestamp = timestamp;
    }

    public BookErrorResponse(List<String> message, LocalDateTime timestamp) {
        this.messageList = message;
        this.timestamp = timestamp;
    }

    public List<String> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<String> messageList) {
        this.messageList = messageList;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }
}
