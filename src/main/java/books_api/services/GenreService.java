package books_api.services;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import books_api.dto.GenreDto;
import books_api.exception_handlers.exceptions.EntityNotFoundException;
import books_api.models.Genre;
import books_api.repositories.GenreRepository;

@Service
@Transactional(readOnly = true)
public class GenreService {

    private final GenreRepository genreRepository;
    private final ModelMapper modelMapper;


    @Autowired
    public GenreService(GenreRepository genreRepository, ModelMapper modelMapper) {
        this.genreRepository = genreRepository;
        this.modelMapper = modelMapper;
    }
    
    @Transactional
    public GenreDto getGenreByName(String name) {
        Genre genre = genreRepository.findByGenreName(name).orElseThrow(() -> new EntityNotFoundException("Genre with name: " + name + " is not found"));
        return modelMapper.map(genre, GenreDto.class);
    }
}
