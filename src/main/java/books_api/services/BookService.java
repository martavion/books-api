package books_api.services;

import books_api.dto.BookDto;
import books_api.exception_handlers.exceptions.DuplicatedEntityException;
import books_api.exception_handlers.exceptions.EntityNotFoundException;
import books_api.exception_handlers.exceptions.EntityNotSavedException;
import books_api.models.Book;
import books_api.repositories.BookRepository;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeMap;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional(readOnly = true)
public class BookService {

    private final BookRepository bookRepository;
    private final ModelMapper modelMapper;
    private final GenreService genreService;

    @Autowired
    public BookService(BookRepository bookRepository, ModelMapper modelMapper, GenreService genreService) {
        this.bookRepository = bookRepository;
        this.modelMapper = modelMapper;
        this.genreService = genreService;
    }

    @Transactional
    public Long create(BookDto bookDto) {
        if (bookDto.getId() != null) {
            bookRepository.findById(bookDto.getId()).ifPresent(book -> {
                throw new DuplicatedEntityException("Entity Book with id " + book.getBookId() + " already exists");
            });
        }
        if (bookDto.getPublishingYear() <= 1900) {
            throw new EntityNotSavedException("Publishing year should be > 1900");
        }
        Book book = fromDto(bookDto);
        enrichBook(book);
        return bookRepository.save(book).getBookId();
    }

    public List<BookDto> getAllBooks() {
        return bookRepository.findAll().stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    public BookDto getBooksById(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book with id: " + id + " is not found"));
        return toDto(book);
    }

    @Transactional
    public BookDto deleteBookById(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book with id: " + id + " is not found"));
        bookRepository.deleteById(id);
        return toDto(book);
    }

    @Transactional
    public BookDto updatedBookById(Long id, BookDto bookDto) {
        Book existingBook = bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book with id: " + id + " is not found"));

        Book book = fromDto(bookDto);

        copyNonNullProperties(book, existingBook);

        existingBook.setUpdatedAt(new Date());

        Book updatedBook = bookRepository.save(existingBook);

        return toDto(updatedBook);
    }


    private void enrichBook(Book book) {
        book.setCreatedAt(new Date());
        book.setUpdatedAt(new Date());
    }

    public BookDto toDto(Book book) {
        // add deep mapping to flatten source's Player object into a single field in destination
        TypeMap<Book, BookDto> propertyMapper = this.modelMapper.getTypeMap(Book.class, BookDto.class);

        if (propertyMapper == null) {
            propertyMapper = this.modelMapper.createTypeMap(Book.class, BookDto.class);
            propertyMapper.addMappings(
                    mapper -> mapper.map(src -> src.getGenre().getGenreName(), BookDto::setGenre));
        }

        return propertyMapper.map(book);
    }

    public Book fromDto(BookDto bookDto) {
        TypeMap<BookDto, Book> propertyMapper = this.modelMapper.getTypeMap(BookDto.class, Book.class);

        if (propertyMapper == null) {
            propertyMapper = this.modelMapper.createTypeMap(BookDto.class, Book.class);
        }

        propertyMapper.addMappings(
                mapper -> mapper.map(src -> genreService.getGenreByName(bookDto.getGenre()), Book::setGenre));

        return propertyMapper.map(bookDto);
    }

    private static void copyNonNullProperties(Object src, Object target) {
        BeanUtils.copyProperties(src, target, getNullPropertyNames(src));
    }

    private static String[] getNullPropertyNames(Object source) {
        final BeanWrapper src = new BeanWrapperImpl(source);
        java.beans.PropertyDescriptor[] pds = src.getPropertyDescriptors();

        Set<String> emptyNames = new HashSet<String>();
        for (java.beans.PropertyDescriptor pd : pds) {
            Object srcValue = src.getPropertyValue(pd.getName());
            if (srcValue == null) emptyNames.add(pd.getName());
        }
        String[] result = new String[emptyNames.size()];
        return emptyNames.toArray(result);
    }
}
