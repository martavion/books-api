package books_api.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import books_api.dto.BookDto;
import books_api.services.BookService;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private BookService booksService;

    @Operation(summary = "Add a new book")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created the book"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content)})
    @PostMapping(value = "/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> postBook(@RequestBody @Valid BookDto book) {
        Long bookId = booksService.create(book);
        return ResponseEntity.status(HttpStatus.CREATED).body("Book with id " + bookId + " is added successfully");
    }

    @Operation(summary = "Gets the list of available books")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the books",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = List.class))}),
    })
    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<BookDto>> getSavedBooks() {
        List<BookDto> allSavedBooks = booksService.getAllBooks();
        return ResponseEntity.status(HttpStatus.OK).body(allSavedBooks);
    }

    @Operation(summary = "Get a book by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK",
                    content = {@Content(mediaType = MediaType.APPLICATION_JSON_VALUE,
                            schema = @Schema(implementation = BookDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Book not found",
                    content = @Content)})
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getSavedBook(@Valid @PathVariable("id") @Min(1) Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(booksService.getBooksById(id));
    }

    @Operation(summary = "Update a book by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Updated the book"),
            @ApiResponse(responseCode = "400", description = "Bad request",
                    content = @Content),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content)})
    @PatchMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateBook(@Valid @PathVariable("id") @Min(1) Long id,
                                             @RequestBody BookDto bookDTO) {
        BookDto updatedBookDto = booksService.updatedBookById(id, bookDTO);
        return ResponseEntity.status(HttpStatus.CREATED).body(updatedBookDto);
    }

    @Operation(summary = "Remove a book by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "No Content"),
            @ApiResponse(responseCode = "400", description = "Bad request"),
            @ApiResponse(responseCode = "404", description = "Not Found"),
            @ApiResponse(responseCode = "500", description = "Internal Server Error",
                    content = @Content)})
    @DeleteMapping(value = "/delete/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteBook(@Valid @PathVariable("id") @Min(1) Long id) {
        BookDto removedBookDto = booksService.deleteBookById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Book with id: " + removedBookDto.getId() + " is removed successfully");
    }

}
