package books_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import books_api.models.Book;

import java.util.List;

@Repository
public interface BookRepository extends JpaRepository<Book, Long> {
    //custom requests to DB
    List<Book> findByTitle(String name);

    List<Book> findByGenreOrderByPublishingYear(String genre);

    List<Book> findByTitleStartingWith(String str);
}
