package books_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import books_api.models.Genre;

import java.util.Optional;

public interface GenreRepository extends JpaRepository<Genre, Long> {
    //custom requests to DB
    Optional<Genre> findByGenreName(String genreName);
}
