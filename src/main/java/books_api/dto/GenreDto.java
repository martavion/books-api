package books_api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class GenreDto {
  
    @JsonProperty("id")
    @NotNull(message = "Id is a mandatory field")
    private Long id;

    @JsonProperty("genre")
    @NotEmpty(message = "Genre is a mandatory field")
    private String genreName;

    public GenreDto() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GenreDto(Long id, String genreName) {
        this.id = id;
        this.genreName = genreName;
    }

    public String getGenreName() {
        return genreName;
    }

    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }
}
