package books_api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class BookDto {
    @JsonProperty("book_id")
    private Long id;

    @JsonProperty("title")
    @NotBlank(message = "Title is a mandatory field")
    private String title;

    @JsonProperty("author")
    @NotBlank(message = "Author is a mandatory field")
    private String author;

    @JsonProperty("genre")
    @Valid
    @NotNull(message = "Genre is a mandatory field")
    private String genre;

    @JsonProperty("publishing_year")
    @NotNull(message = "Publishing year is a mandatory field")
    @Min(1900)
    private Integer publishingYear;

    public BookDto() {

    }

    public BookDto(Long bookId, String title, String author, String genre, Integer publishingYear) {
        this.id = bookId;
        this.title = title;
        this.author = author;
        this.genre = genre;
        this.publishingYear = publishingYear;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getGenre() {
        return this.genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public Integer getPublishingYear() {
        return publishingYear;
    }

    public void setPublishingYear(Integer year) {
        this.publishingYear = year;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static class Builder {
        private Long id;
        private String title;
        private String author;
        private String genre;
        private Integer publishingYear;

        public Builder withBookId(Long bookId) {
            this.id = bookId;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withAuthor(String author) {
            this.author = author;
            return this;
        }

        public Builder withGenre(String genre) {
            this.genre = genre;
            return this;
        }

        public Builder withPublishingYear(Integer publishingYear) {
            this.publishingYear = publishingYear;
            return this;
        }

        public BookDto build() {
            BookDto bookDto = new BookDto();
            bookDto.id = this.id;
            bookDto.title = this.title;
            bookDto.author = this.author;
            bookDto.genre = this.genre;
            bookDto.publishingYear = this.publishingYear;
            return bookDto;
        }
    }
}
