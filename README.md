# Set up

* Mandatory requirements are [maven](https://maven.apache.org/install.html) and jdk [17](https://docs.aws.amazon.com/corretto/latest/corretto-17-ug/downloads-list.html).
* Database configuration. 
   Download docker image postgres:latest and start container. Create empty database with name 'books_db' when you run first
*  Access to the database. Hostname: 127.0.01:5432, database: postgres, user: postgres. password: admin

# Run API locally: 
* Run command: mvn spring-boot:run
* To build jar file: mvn clean install spring-boot:repackage 

# Open API documentation
* Open http:http://localhost:8080/swagger-ui/index.html in browser





